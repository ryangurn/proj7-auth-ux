# Project 7: Authentication Added

Adding Authentication to the rest of the project.
## About Me
###### Name: Ryan Gurnick
###### Email: rgurnick@uoregon.edu

## Setup
1. Clone the repo
2. Add `credentials.ini` into the root directory of the application.
3. Add `credentials.ini` to the Auth directory.

(This will ensure that the credentials.ini file will exist in all places that it needs to.)
4. Run `docker-compose up --build` from the `root` folder.
5. Navigate to `localhost:5001` to add brevet data into the normal web form from p5.
6. Navigate to `localhost:5001/listAll` or any of the other URIs listed under functionality, to ensure things are working correctly.

## Functionality
This application is a clone with additional features of project 4. The following additional features are listed below:

* Rest URI for accessing data:
    
    * "http://<host:port>/api/users" allows post requests to create a user
    
    - It requires application/json data with a `email`, and `password` provided
    
    * "http://<host:port>/api/users/<id>" allows you to get a specific user given an id
    
    * "http://<host:port>/api/token" allows you to generate a token to use for basic authentication, provide the token as the username for HTTPbasic authentication if wanted.
    
    * "http://<host:port>/listAll" should return all open and close times in the database
    
    * "http://<host:port>/listOpenOnly" should return open times only
    
    * "http://<host:port>/listCloseOnly" should return close times only
    
    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format
    
    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format
    
    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format
    
    * Dockerfile (for laptop)
    
    * docker-compose.yml
