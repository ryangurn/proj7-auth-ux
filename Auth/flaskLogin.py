"""
Flask-Login example
===================
This is a small application that provides a trivial demonstration of
Flask-Login, including remember me functionality.

:copyright: (C) 2011 by Matthew Frazier.
:license: MIT/X11, see LICENSE for more details.
:source: https://gist.github.com/robbintt/32074560e52e46788237
"""
import pymongo
from pymongo import MongoClient
import os
import arrow
import datetime

import acp_times
import config
from flask import Flask, render_template, request, redirect, url_for, flash, jsonify, session, abort, g
from flask_mongoengine import MongoEngine, Document
from flask_httpauth import HTTPBasicAuth
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import Email, Length, InputRequired, Optional
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)

app = Flask(__name__)
CONFIG = config.configuration()
auth = HTTPBasicAuth()

app.config['MONGODB_SETTINGS'] = {
    'host': 'mongodb://'+os.environ['DB_PORT_27017_TCP_ADDR']+':27017/brevet'
}

db = MongoEngine(app)
app.config['SECRET_KEY'] = CONFIG.SECRET_KEY
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

client = MongoClient('mongodb://'+os.environ['DB_PORT_27017_TCP_ADDR']+':27017/brevet')
pydb = client.brevet


class User(UserMixin, db.Document):
    meta = {'collection': 'users'}
    email = db.StringField(max_length=30)
    password = db.StringField()

    def generate_auth_token(self, expiration=600):
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': str(self.id)})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None  # valid token, but expired
        except BadSignature:
            return None  # invalid token
        # user = User.query.get(data['id'])
        user = User.objects(email=data['id']).first()
        return user


@login_manager.user_loader
def load_user(user_id):
    return User.objects(pk=user_id).first()


class RegForm(FlaskForm):
    email = StringField('email', validators=[InputRequired(message="Email required"), Email(message='Invalid email'),
                                             Length(max=45, message="Email must be at most 45 characters")])
    password = PasswordField('password', validators=[InputRequired(message="Password required"), Length(min=8, max=20,
                                                                                                        message="Password must be between 8 and 20 characters")])


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegForm()
    if request.method == 'POST':
        if form.validate():
            existing_user = User.objects(email=form.email.data).first()
            if existing_user is None:
                hashpass = generate_password_hash(form.password.data, method='sha256')
                hey = User(form.email.data, hashpass).save()
                login_user(hey)
                return redirect(url_for('dashboard'))
        else:
            for ee in form.email.errors:
                flash("Email: " + ee)
            for pe in form.password.errors:
                flash("Password: " + pe)
    return render_template('register.html', form=form)


class LoginForm(FlaskForm):
    email = StringField('email', validators=[InputRequired(), Email(message='Invalid email'), Length(max=30)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=20)])
    remember = BooleanField('remember', validators=[Optional()])


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('dashboard'))
    form = LoginForm()
    if request.method == 'POST':
        if form.validate():
            check_user = User.objects(email=form.email.data).first()
            if check_user:
                if check_password_hash(check_user['password'], form.password.data):
                    if form.remember.data:
                        login_user(check_user, remember=True)
                    else:
                        login_user(check_user, remember=False)
                    # return redirect(url_for('dashboard'))
                    return redirect(request.args.get("next") or url_for("index"))
                flash("Wrong username or password!")
            flash("Wrong username or password!")
        else:
            for ee in form.email.errors:
                flash("Email: " + ee)
            if len(form.password.errors) >= 1:
                flash("Password: Incorrect password")
    return render_template('login.html', form=form)


@app.route('/dashboard')
@login_required
def dashboard():
    return render_template('dashboard.html', name=current_user.email)


@app.route('/logout', methods=['GET'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))


#####
#
# Main Index & Error Handlers
#
#####

# Index
@app.route("/")
@app.route("/index")
@login_required
def index():
    count = pydb.data.find({}).count()
    g = True

    if count <= 0:
        g = False

    app.logger.debug("Main page entry")
    return render_template('calc.html', get=g)


# Error Handlers
@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    session['linkback'] = url_for("index")
    return render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
@login_required
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('dist', 200, type=int)
    start = request.args.get('start', None, type=str)
    app.logger.debug("km={}".format(km))
    app.logger.debug("dist={}".format(dist))
    app.logger.debug("start={}".format(start))
    app.logger.debug("request.args: {}".format(request.args))

    a = arrow.get(start, 'YYYY-MM-DD HH:mm', tzinfo='US/Pacific')

    open_time = acp_times.open_time(km, dist, a.isoformat())
    close_time = acp_times.close_time(km, dist, a.isoformat())
    result = {"open": open_time, "close": close_time}
    return jsonify(result=result)


@app.route('/submit', methods=['POST'])
@login_required
def submit():
    """
    Submits the data from the form to the mongodb database
    this will ensure that the data is stored for future use
    for the API and act as a set of digest logic for the db.
    :return: None
    """
    data = request.form
    iterator = 0
    dist = data.get('distance')
    begin_date = data.get('begin_date')
    begin_time = data.get('begin_time')

    # organize the data into an array
    miles = []
    km = []
    open = []
    close = []
    length = 0
    for v in data:
        split = v.split("_", 1)
        if split[0] != 'begin':  # strip out begin time and begin date
            if len(split) == 2:  # remove distance
                key = split[1]  # index
                value = split[0]  # miles/km/open/close
                if data["_".join(split)] != "":  # remove empty input slots
                    if value == "miles":
                        miles.append(data["_".join(split)])
                        length += 1 / 4
                    elif value == "km":
                        km.append(data["_".join(split)])
                        length += 1 / 4
                    elif value == "close":
                        close.append(data["_".join(split)])
                        length += 1 / 4
                    elif value == "open":
                        open.append(data["_".join(split)])
                        length += 1 / 4
                    print(length, key, value, "_".join(split), data["_".join(split)])
    length = int(length)

    if length == 0:
        return redirect('/')

    output = {}
    for i in range(0, length):
        output.update({str(i): {"miles": miles[i], "km": km[i], "open": open[i], "close": close[i]}})

    pydb.data.insert_one(
        {'data': output, 'created': datetime.datetime.utcnow(), 'distance': dist, 'begin_date': begin_date,
         'begin_time': begin_time})
    return redirect('/')


@app.route('/get')
@login_required
def get():
    """
    Allow the user to get a view of the data that is currently being
    stored within mongo for use by the API. This will return a view
    to the user of the most recent set of brevet calculations.
    :return:
    """
    data = pydb.data.find().sort('created', pymongo.DESCENDING).limit(1)
    newData = None
    for d in data:
        newData = d

    viewData = newData['data']
    distance = newData['distance']

    return render_template('get.html', distance=distance, data=viewData)


###############
#
# REST API Handler
#
#
###############
@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = User.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.objects(email=username_or_token).first()
        # user = User.query.filter_by(username=username_or_token).first()
        if not user or not check_password_hash(user['password'], password):
            return False
    g.user = user
    return True


@app.route('/api/users', methods=['POST'])
def new_user():
    email = request.json.get('email')
    password = request.json.get('password')
    if email is None or password is None:
        abort(400)    # missing arguments
    if User.objects(email=email).first() is not None:
        abort(400)    # existing user
    hashpass = generate_password_hash(password, method='sha256')
    hey = User(email, hashpass).save()
    return (jsonify({'username': hey.email}), 201,
            {'Location': url_for('get_user', id=str(hey.id), _external=True)})


@app.route('/api/users/<id>')
def get_user(id):
    user = User.objects(_id=id).first()
    if not user:
        abort(400)
    return jsonify({'email': user.email})


@app.route('/api/token')
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token(600)
    return jsonify({'token': token.decode('ascii'), 'duration': 600})


@app.route('/api/resource')
@auth.login_required
def get_resource():
    return jsonify({'data': 'Hello, %s!' % g.user.email})


def __listAll(removeT=None, t='json', top=None):
    data = pydb.data.find({})

    if t is 'json':
        newData = []
        for d in data:
            obj = {
                'id': str(d['_id'])
            }
            count = 0
            for a in d['data']:
                if removeT is not None:
                    del d['data'][a][removeT]
                if top is None:
                    obj.update({a: d['data'][a]})
                elif count <= (int(top) - 1):
                    obj.update({a: d['data'][a]})
                    count += 1
            newData.append(obj)
        return jsonify(newData)
    elif t is 'csv':
        if removeT is 'open':
            retStr = 'id,miles,km,close,\n'
        elif removeT is 'close':
            retStr = 'id,miles,km,open,\n'
        elif removeT is None:
            retStr = 'id,miles,km,open,close,\n'
        for d in data:
            for a in d['data']:
                if removeT is not None:
                    del d['data'][a][removeT]
                retStr += str(d['_id']) + ","
                for dd in d['data'][a]:
                    retStr += str(d['data'][a][dd]) + ","
                retStr += "\n"
        return retStr


@app.route('/listAll')
@auth.login_required
def listAll():
    top = request.args.get('top')
    return __listAll(top=top)


@app.route('/listAll/json')
@auth.login_required
def listAllJSON():
    top = request.args.get('top')
    return __listAll(top=top)


@app.route('/listAll/csv')
@auth.login_required
def listAllCSV():
    top = request.args.get('top')
    return __listAll(t='csv', top=top)


@app.route('/listOpenOnly')
@auth.login_required
def listOpenOnly():
    top = request.args.get('top')
    return __listAll('close', 'json', top)


@app.route('/listOpenOnly/json')
@auth.login_required
def listOpenOnlyJSON():
    top = request.args.get('top')
    return __listAll('close', 'json', top)


@app.route('/listOpenOnly/csv')
@auth.login_required
def listOpenOnlyCSV():
    top = request.args.get('top')
    return __listAll('close', 'csv', top)


@app.route('/listCloseOnly')
@auth.login_required
def listCloseOnly():
    top = request.args.get('top')
    return __listAll('open', 'json', top)


@app.route('/listCloseOnly/json')
@auth.login_required
def listCloseOnlyJSON():
    top = request.args.get('top')
    return __listAll('open', 'json', top)


@app.route('/listCloseOnly/csv')
@auth.login_required
def listCloseOnlyCSV():
    top = request.args.get('top')
    return __listAll('open', 'csv', top)


if __name__ == '__main__':
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(host='0.0.0.0', port=CONFIG.PORT, debug=CONFIG.PORT)
